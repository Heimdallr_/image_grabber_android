package ru.fate.imagegrabber.android.server.models.common

import com.google.gson.annotations.SerializedName

/**
 * Created by Heimdallr on 08.02.2017.
 */
class BaseDataListResp<T : Any> : BaseResp() {
    @SerializedName("data")
    var data: List<T> = ArrayList()
}