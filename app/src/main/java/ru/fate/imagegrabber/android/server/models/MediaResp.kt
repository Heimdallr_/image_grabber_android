package ru.fate.imagegrabber.android.server.models

import com.google.gson.annotations.SerializedName
import ru.fate.imagegrabber.android.server.models.common.BaseEntityResp2

/**
 * Created by Heimdallr on 24.07.2017.
 */
class MediaResp : BaseEntityResp2() {
    @SerializedName("link")
    lateinit var link: String

    @SerializedName("views")
    var totalViews: Long = 0

    @SerializedName("is_album")
    var isAlbum: Boolean = false

    @SerializedName("title")
    var title: String? = null

    @SerializedName("description")
    var description: String? = null

    @SerializedName("images")
    var images: List<ImageResp> = ArrayList()
}