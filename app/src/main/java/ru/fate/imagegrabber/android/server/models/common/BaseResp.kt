package ru.fate.imagegrabber.android.server.models.common

import com.google.gson.annotations.SerializedName

/**
 * Created by Heimdallr on 08.02.2017.
 */
open class BaseResp {
    @SerializedName("status")
    var status: Int = 0

    @SerializedName("success")
    var isSuccess: Boolean = false
}