package ru.fate.imagegrabber.android.utils;

import android.content.res.Resources;

import io.reactivex.exceptions.Exceptions;
import ru.fate.imagegrabber.android.server.ApiException;
import ru.fate.imagegrabber.android.server.models.common.BaseResp;


/**
 * Created by Heimdallr on 11.02.2017.
 */
public class Utils {
    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }

    public static void checkResponseForError(BaseResp response) {
        if (!response.isSuccess()) {
            Exceptions.propagate(new ApiException(response.getStatus()));
        }
    }
}