package ru.fate.imagegrabber.android.common.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_media.view.*
import ru.fate.imagegrabber.android.R
import ru.fate.imagegrabber.android.common.models.MediaModel

class MediaRealmAdapter(data: OrderedRealmCollection<MediaModel>?, private val context: Context, private val onItemClickListener: OnItemClickListener) : RealmRecyclerViewAdapter<MediaModel, MediaRealmAdapter.ViewHolder>(data, true) {
    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder = ViewHolder(LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_media, viewGroup, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(data!![position])

    override fun getItemCount(): Int = data?.size ?: 0

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        init {
            itemView.setOnClickListener {
                if (adapterPosition >= 0 && adapterPosition < data!!.size) {
                    onItemClickListener.onItemClick(adapterPosition, data!![adapterPosition])
                }
            }
        }

        fun bind(model: MediaModel) {
            if (model.isAlbum) {
                Glide.with(context)
                        .load(model.images[0].link)
                        .into(itemView.ivPhoto)
            } else {
                Glide.with(context)
                        .load(model.link)
                        .into(itemView.ivPhoto)
            }

        }
    }

    interface OnItemClickListener {
        fun onItemClick(position: Int, model: MediaModel)
    }
}