package ru.fate.imagegrabber.android.app

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import ru.fate.imagegrabber.android.R
import ru.fate.imagegrabber.android.app.media.MediaPresenter
import ru.fate.imagegrabber.android.app.media.MediaRouter
import ru.fate.imagegrabber.android.app.media.MediaView

/**
 * Created by Heimdallr on 26.09.2017.
 */
class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            val view = MediaView()
            view.presenter = MediaPresenter(view, MediaRouter(this))
            supportFragmentManager.beginTransaction()
                    .add(getFragmentContainerId(), view, view.javaClass.simpleName)
                    .commit()
        }
    }

    fun getFragmentContainerId(): Int = view_container.id
}