package ru.fate.imagegrabber.android.app.media

import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.StaggeredGridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.layout_toolbar_with_title.view.*
import kotlinx.android.synthetic.main.view_media.view.*
import ru.fate.imagegrabber.android.R
import ru.fate.imagegrabber.android.common.BaseControllerWithPresenter
import ru.fate.imagegrabber.android.utils.Utils
import ru.fate.imagegrabber.android.views.EndlessRecyclerViewScrollListener
import ru.fate.imagegrabber.android.views.GridSpacingItemDecoration

class MediaView : BaseControllerWithPresenter<MediaPresenter>() {
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.view_media, container, false)

    //region Реализованные методы вида
    fun setRefreshing(refreshing: Boolean) {
        view?.swipe?.isRefreshing = refreshing
    }

    fun setAdapter(adapter: RecyclerView.Adapter<*>) {
        view?.recycler?.adapter = adapter
    }
    //endregion

    override fun prepareUi() {
        view?.toolbarTitle?.text = getString(R.string.gallery)

        view?.swipe?.setColorSchemeResources(R.color.colorAccent, R.color.colorAccent, R.color.colorPrimary)
        view?.swipe?.setOnRefreshListener { presenter?.onRefreshPerformed() }

        view?.recycler?.let {
            it.itemAnimator = null
            val gridLayoutManager = StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL)
            it.layoutManager = gridLayoutManager
            it.addItemDecoration(GridSpacingItemDecoration(2, Utils.dpToPx(15), true))
            it.addOnScrollListener(object : EndlessRecyclerViewScrollListener(gridLayoutManager) {
                override fun onLoadMore(page: Int, totalItemsCount: Int) {
                    presenter?.onLoadMorePerformed()
                    reset()
                }
            })
        }
    }
}