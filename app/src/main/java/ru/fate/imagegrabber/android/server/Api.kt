package ru.fate.imagegrabber.android.server

import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path
import ru.fate.imagegrabber.android.server.models.CommentResp
import ru.fate.imagegrabber.android.server.models.MediaResp
import ru.fate.imagegrabber.android.server.models.common.BaseDataListResp
import ru.fate.imagegrabber.android.server.models.common.BaseDataObjectResp

interface Api {
    companion object {
        val API_RELEASE_URL = "https://api.imgur.com/3/"
        val API_DEBUG_URL = "https://api.imgur.com/3/"
    }

    @GET("gallery/top/viral/all/{page}?album_previews=true")
    fun getMedias(@Path("page") page: Long): Flowable<BaseDataListResp<MediaResp>>

    @GET("gallery/image/{image_id}")
    fun getImageInfo(@Path("image_id") imageId: String): Flowable<BaseDataObjectResp<MediaResp>>

    @GET("gallery/album/{album_id}")
    fun getAlbumInfo(@Path("album_id") albumId: String): Flowable<BaseDataObjectResp<MediaResp>>

    @GET("gallery/{media_id}/comments")
    fun getComments(@Path("media_id") mediaId: String): Flowable<BaseDataListResp<CommentResp>>
}