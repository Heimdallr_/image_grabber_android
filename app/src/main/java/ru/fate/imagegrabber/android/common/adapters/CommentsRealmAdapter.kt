package ru.fate.imagegrabber.android.common.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.realm.OrderedRealmCollection
import io.realm.RealmRecyclerViewAdapter
import kotlinx.android.synthetic.main.item_comment.view.*
import ru.fate.imagegrabber.android.R
import ru.fate.imagegrabber.android.common.models.CommentModel

class CommentsRealmAdapter(data: OrderedRealmCollection<CommentModel>?, private val context: Context) : RealmRecyclerViewAdapter<CommentModel, CommentsRealmAdapter.ViewHolder>(data, true) {
    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): CommentsRealmAdapter.ViewHolder = ViewHolder(LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_comment, viewGroup, false))

    override fun onBindViewHolder(holder: CommentsRealmAdapter.ViewHolder, position: Int) = holder.bind(data!![position])

    override fun getItemCount(): Int {
        data?.let {
            if (it.size > 3) {
                return 3
            }
            return it.size
        }
        return 0
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(model: CommentModel) {
            itemView.tvName.text = model.authorName
            itemView.tvComment.text = model.text
        }
    }
}