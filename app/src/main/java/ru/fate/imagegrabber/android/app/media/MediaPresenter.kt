package ru.fate.imagegrabber.android.app.media

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import ru.fate.imagegrabber.android.R
import ru.fate.imagegrabber.android.common.BasePresenter
import ru.fate.imagegrabber.android.common.adapters.MediaRealmAdapter
import ru.fate.imagegrabber.android.common.interactors.GalleryInteractor
import ru.fate.imagegrabber.android.common.models.MediaModel
import ru.fate.imagegrabber.android.server.ApiException

/**
 * Created by Heimdallr on 19.08.2017.
 */
class MediaPresenter(wheelAlbumView: MediaView, wheelAlbumRouter: MediaRouter) : BasePresenter<MediaView, MediaRouter>(wheelAlbumView, wheelAlbumRouter) {
    private val galleryInteractor = GalleryInteractor()

    private var getMediaSub = Disposables.disposed()

    private lateinit var adapter: MediaRealmAdapter

    override fun onStart() {
        val images = galleryInteractor.getImagesFromDb()
        adapter = MediaRealmAdapter(images, view.activity!!, object : MediaRealmAdapter.OnItemClickListener {
            override fun onItemClick(position: Int, model: MediaModel) {
                router.toMediaDetail(model.id, model.isAlbum)
            }
        })
        view.setAdapter(adapter)
        if (adapter.itemCount <= 0) {
            loadImages(true)
        }
    }

    override fun onStop() {
        view.setRefreshing(false)
        getMediaSub.dispose()
    }

    //region Приватные методы
    private fun loadImages(newQuery: Boolean) {
        if (!getMediaSub.isDisposed) {
            return
        }
        view.setRefreshing(true)
        getMediaSub = galleryInteractor.getMedias(if (newQuery) 0 else (adapter.itemCount / 60).toLong())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    if (newQuery) {
                        galleryInteractor.deleteImagesFromDb()
                    }
                    galleryInteractor.saveImagesToDb(it)
                }, {
                    view.setRefreshing(false)
                    val cause = it.cause
                    if (cause != null && cause is ApiException) {
                        view.showError(R.string.error_has_occurred)
                        return@subscribe
                    }
                    it.printStackTrace()
                    view.showError(R.string.can_not_connect_to_server)
                }) { view.setRefreshing(false) }
    }
    //endregion

    //region Методы для вызова из вида
    fun onRefreshPerformed() = loadImages(true)

    fun onLoadMorePerformed() = loadImages(false)
    //endregion
}