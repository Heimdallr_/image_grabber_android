package ru.fate.imagegrabber.android.common.interactors

import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import ru.fate.imagegrabber.android.common.models.CommentModel
import ru.fate.imagegrabber.android.utils.Utils

/**
 * Created by Heimdallr on 19.08.2017.
 */
class CommentsInteractor : BaseInteractor() {
    fun getComments(mediaId: String): Flowable<List<CommentModel>> {
        return api.getComments(mediaId).subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .flatMap {
                    Utils.checkResponseForError(it)
                    Flowable.fromIterable(it.data)
                }
                .map(CommentModel.Companion::convert)
                .toList().toFlowable()
    }

    fun deleteCommentsFromDb(mediaId: String) {
        Realm.getDefaultInstance().executeTransaction {
            it.where(CommentModel::class.java)
                    .equalTo("mediaId", mediaId)
                    .findAll().deleteAllFromRealm()
        }
    }

    fun saveCommentsToDb(comments: List<CommentModel>) {
        Realm.getDefaultInstance().executeTransaction { it.copyToRealmOrUpdate(comments) }
    }

    fun getLastCommentsFromDb(mediaId: String): RealmResults<CommentModel> {
        return Realm.getDefaultInstance().where(CommentModel::class.java)
                .equalTo("mediaId", mediaId)
                .findAllSorted("publicationDate", Sort.ASCENDING)
    }
}