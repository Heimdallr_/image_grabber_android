package ru.fate.imagegrabber.android.common.models

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import ru.fate.imagegrabber.android.server.models.MediaResp

/**
 * Created by Heimdallr on 24.07.2017.
 */
open class MediaModel : RealmObject() {
    @PrimaryKey
    lateinit var id: String
    lateinit var link: String
    var totalViews: Long = 0
    var isAlbum: Boolean = false
    var title: String? = null
    var description: String? = null
    var images: RealmList<ImageModel> = RealmList()

    companion object {
        fun convert(response: MediaResp): MediaModel {
            val model = MediaModel()
            model.id = response.id
            model.link = response.link
            model.totalViews = response.totalViews
            model.isAlbum = response.isAlbum
            model.title = response.title
            model.description = response.description
            model.images = RealmList(*ImageModel.convert(response.images).toTypedArray())
            return model
        }

        fun convert(response: List<MediaResp>): List<MediaModel> =
                response.map { convert(it) }
    }
}