package ru.fate.imagegrabber.android;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.Hashtable;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import okhttp3.Cache;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.fate.imagegrabber.android.server.Api;

/**
 * Created by Heimdallr on 01.02.2017.
 */
public class App extends MultiDexApplication {
    public static final String TYPEFACE_SFNS_DISPLAY = "SFNS-Display.ttf";

    private static final Hashtable<String, Typeface> typeFaces = new Hashtable<>(1);

    private static App app;
    private static Api api;

    @Override protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override public void onCreate() {
        super.onCreate();
        app = this;

        Realm.init(this);
        RealmConfiguration config = new RealmConfiguration.Builder()
                .deleteRealmIfMigrationNeeded()
                .build();
        Realm.setDefaultConfiguration(config);
    }

    public static App getInstance() {
        return app;
    }

    public static Typeface getTypeFace(String fileName) {
        Typeface typeface = typeFaces.get(fileName);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(getInstance().getAssets(), fileName);
            typeFaces.put(fileName, typeface);
        }
        return typeface;
    }

    public static Api getApi() {
        if (api == null) {
            String baseUrl;
            if (BuildConfig.DEBUG) {
                baseUrl = Api.Companion.getAPI_DEBUG_URL();
            } else {
                baseUrl = Api.Companion.getAPI_RELEASE_URL();
            }
            api = provideApi(baseUrl);
        }
        return api;
    }

    private static Cache provideHttpCache(Application application) {
        return new Cache(application.getCacheDir(), 10 * 1024 * 1024);
    }

    private static Gson provideGson() {
        return new GsonBuilder().setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                .create();
    }

    private static OkHttpClient provideHttpClient(Cache cache) {
        HttpLoggingInterceptor logInterceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG) {
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        } else {
            logInterceptor.setLevel(HttpLoggingInterceptor.Level.NONE);
        }
        return new OkHttpClient.Builder()
                .addInterceptor(logInterceptor)
                .addInterceptor(chain -> {
                    Request original = chain.request();
                    Request.Builder requestBuilder = original.newBuilder()
                            .addHeader("Authorization", "Client-ID " + String.format(getInstance().getString(R.string.imgur_client_id)));
                    Request request = requestBuilder.build();
                    return chain.proceed(request);
                })
                .cache(cache)
                .build();
    }

    private static Api provideApi(String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(provideGson()))
                .client(provideHttpClient(provideHttpCache(getInstance())))
                .build().create(Api.class);
    }
}