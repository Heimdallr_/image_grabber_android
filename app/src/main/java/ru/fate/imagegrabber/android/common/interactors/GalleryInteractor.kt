package ru.fate.imagegrabber.android.common.interactors

import io.reactivex.Flowable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmResults
import io.realm.Sort
import ru.fate.imagegrabber.android.common.models.MediaModel
import ru.fate.imagegrabber.android.utils.Utils

/**
 * Created by Heimdallr on 19.08.2017.
 */
class GalleryInteractor : BaseInteractor() {
    fun getImagesFromDb(): RealmResults<MediaModel> = Realm.getDefaultInstance().where(MediaModel::class.java)
            .findAllSorted("totalViews", Sort.DESCENDING)

    fun getMedias(page: Long): Flowable<List<MediaModel>> {
        return api.getMedias(page)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .flatMap {
                    Utils.checkResponseForError(it)
                    Flowable.fromIterable(it.data)
                }
                .map { MediaModel.convert(it) }
                .toList().toFlowable()
    }

    fun deleteImagesFromDb() {
        Realm.getDefaultInstance().executeTransaction {
            it.where(MediaModel::class.java)
                    .findAll().deleteAllFromRealm()
        }
    }

    fun saveImagesToDb(models: List<MediaModel>) {
        Realm.getDefaultInstance().executeTransaction { it.copyToRealmOrUpdate(models) }
    }

    fun getMediaInfo(mediaId: String, isAlbum: Boolean?): Flowable<MediaModel> {
        return if (isAlbum!!) {
            api.getAlbumInfo(mediaId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(Schedulers.io())
                    .doOnNext { Utils.checkResponseForError(it) }
                    .map { MediaModel.convert(it.data) }
        } else api.getImageInfo(mediaId)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnNext { Utils.checkResponseForError(it) }
                .map { model -> MediaModel.convert(model.data) }
    }

    fun getMediaFromDb(id: String): MediaModel? =
            Realm.getDefaultInstance().where(MediaModel::class.java).equalTo("id", id).findFirst()

    fun saveMediaToDb(model: MediaModel) {
        Realm.getDefaultInstance().executeTransaction { realm1 -> realm1.copyToRealmOrUpdate(model) }
    }
}