package ru.fate.imagegrabber.android.common.models

import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import ru.fate.imagegrabber.android.server.models.CommentResp

/**
 * Created by Heimdallr on 24.07.2017.
 */
open class CommentModel : RealmObject() {
    @PrimaryKey
    var id: Long = 0
    lateinit var mediaId: String
    var authorId: Long = 0
    var authorName: String? = null
    lateinit var text: String
    var publicationDate: Long = 0

    companion object {
        fun convert(response: CommentResp): CommentModel {
            val model = CommentModel()
            model.id = response.id
            model.mediaId = response.mediaId
            model.authorId = response.authorId
            model.authorName = response.authorName
            model.text = response.text
            model.publicationDate = response.publicationDate
            return model
        }

        fun convert(response: List<CommentResp>): List<CommentModel> =
                response.map { convert(it) }
    }
}