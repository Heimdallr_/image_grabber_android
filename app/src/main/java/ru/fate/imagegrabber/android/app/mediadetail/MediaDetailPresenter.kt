package ru.fate.imagegrabber.android.app.mediadetail

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposables
import io.reactivex.schedulers.Schedulers
import ru.fate.imagegrabber.android.R
import ru.fate.imagegrabber.android.common.BasePresenter
import ru.fate.imagegrabber.android.common.adapters.CommentsRealmAdapter
import ru.fate.imagegrabber.android.common.interactors.CommentsInteractor
import ru.fate.imagegrabber.android.common.interactors.GalleryInteractor
import ru.fate.imagegrabber.android.common.models.MediaModel
import ru.fate.imagegrabber.android.server.ApiException

/**
 * Created by Heimdallr on 19.08.2017.
 */
class MediaDetailPresenter(tuningDetailView: MediaDetailView, tuningDetailRouter: MediaDetailRouter) : BasePresenter<MediaDetailView, MediaDetailRouter>(tuningDetailView, tuningDetailRouter) {
    private val galleryInteractor = GalleryInteractor()
    private val commentsInteractor = CommentsInteractor()

    private var getMediaSub = Disposables.disposed()

    private var media: MediaModel? = null

    private lateinit var commentsAdapter: CommentsRealmAdapter

    override fun onStart() {
        commentsAdapter = CommentsRealmAdapter(null, view.activity!!)
        view.setCommentsAdapter(commentsAdapter)

        media = galleryInteractor.getMediaFromDb(view.mediaId)
        if (media != null) {
            view.setMediaInfo(media!!.title, media!!.description, media!!.totalViews, if (media!!.isAlbum) media!!.images[0].link else media!!.link)

            val comments = commentsInteractor.getLastCommentsFromDb(view.mediaId)
            commentsAdapter.updateData(comments)
        }

        view.setMediaContentVisibility(media != null)

        getMedia()
    }

    override fun onStop() {
        getMediaSub.dispose()
    }

    //region Приватные методы
    private fun getMedia() {
        if (!getMediaSub.isDisposed) {
            return
        }

        view.showProgress()
        getMediaSub = galleryInteractor.getMediaInfo(view.mediaId, view.isAlbum)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .doOnNext { galleryInteractor.saveMediaToDb(it) }
                .observeOn(AndroidSchedulers.mainThread())
                .doOnNext {
                    media = galleryInteractor.getMediaFromDb(view.mediaId)
                    view.setMediaInfo(media!!.title, media!!.description, media!!.totalViews, if (media!!.isAlbum) media!!.images[0].link else media!!.link)
                    view.setMediaContentVisibility(true)
                }
                .observeOn(Schedulers.io())
                .flatMap { commentsInteractor.getComments(view.mediaId) }
                .doOnNext {
                    commentsInteractor.deleteCommentsFromDb(view.mediaId)
                    commentsInteractor.saveCommentsToDb(it)
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ }, {
                    view.hideProgress()
                    val cause = it.cause
                    if (cause != null && cause is ApiException) {
                        view.showError(R.string.error_has_occurred)
                        return@subscribe
                    }
                    it.printStackTrace()
                    view.showError(R.string.can_not_connect_to_server)
                }) { view.hideProgress() }

    }
    //endregion

    //region Публичные методы для вызова из вида
    //endregion
}