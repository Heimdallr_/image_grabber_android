package ru.fate.imagegrabber.android.server.models.common

import com.google.gson.annotations.SerializedName

/**
 * Created by Heimdallr on 08.02.2017.
 */
open class BaseEntityResp2 {
    @SerializedName("id")
    lateinit var id: String
}