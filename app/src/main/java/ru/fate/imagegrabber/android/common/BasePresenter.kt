package ru.fate.imagegrabber.android.common

import android.content.Intent
import android.os.Bundle

/**
 * Created by Heimdallr on 19.08.2017.
 */
//базовый презентер с роутером и вью под viper
abstract class BasePresenter<View, Router>(var view: View, var router: Router) {
    open fun onStart() = Unit

    open fun onStop() = Unit

    open fun onRestoreViewState(view: android.view.View, savedViewState: Bundle) = Unit

    open fun onSaveViewState(view: android.view.View, outState: Bundle) = Unit

    open fun onSaveInstanceState(outState: Bundle) = Unit

    open fun onRestoreInstanceState(savedInstanceState: Bundle) = Unit

    open fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) = Unit
}