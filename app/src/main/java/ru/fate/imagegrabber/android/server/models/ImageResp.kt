package ru.fate.imagegrabber.android.server.models

import com.google.gson.annotations.SerializedName
import ru.fate.imagegrabber.android.server.models.common.BaseEntityResp2

/**
 * Created by Heimdallr on 24.07.2017.
 */
class ImageResp : BaseEntityResp2() {
    @SerializedName("link")
    lateinit var link: String
}