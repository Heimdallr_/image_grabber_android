package ru.fate.imagegrabber.android.common.models

import io.realm.RealmObject
import ru.fate.imagegrabber.android.server.models.ImageResp

/**
 * Created by Heimdallr on 24.07.2017.
 */
open class ImageModel : RealmObject() {
    lateinit var link: String

    companion object {
        fun convert(response: ImageResp): ImageModel {
            val model = ImageModel()
            model.link = response.link
            return model
        }

        fun convert(response: List<ImageResp>): List<ImageModel> = response.map { convert(it) }
    }
}