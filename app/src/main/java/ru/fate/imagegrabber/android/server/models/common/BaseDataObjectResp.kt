package ru.fate.imagegrabber.android.server.models.common

import com.google.gson.annotations.SerializedName

/**
 * Created by Heimdallr on 08.02.2017.
 */
class BaseDataObjectResp<T : Any> : BaseResp() {
    @SerializedName("data")
    lateinit var data: T
}