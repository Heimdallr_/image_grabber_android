package ru.fate.imagegrabber.android.app.media

import ru.fate.imagegrabber.android.app.MainActivity
import ru.fate.imagegrabber.android.app.mediadetail.MediaDetailPresenter
import ru.fate.imagegrabber.android.app.mediadetail.MediaDetailRouter
import ru.fate.imagegrabber.android.app.mediadetail.MediaDetailView
import ru.fate.imagegrabber.android.common.BaseRouter

/**
 * Created by Heimdallr on 22.08.2017.
 */
class MediaRouter(router: MainActivity) : BaseRouter(router) {
    fun toMediaDetail(id: String, isAlbum: Boolean) {
        val view = MediaDetailView.newInstance(id, isAlbum)
        view.presenter = MediaDetailPresenter(view, MediaDetailRouter(activity))

        activity.supportFragmentManager.beginTransaction()
                .replace(activity.getFragmentContainerId(), view, view.javaClass.simpleName)
                .addToBackStack(null).commit()
    }
}