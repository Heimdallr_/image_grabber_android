package ru.fate.imagegrabber.android.common

import android.app.Dialog
import android.content.ContentResolver
import android.content.Context
import android.graphics.Color
import android.support.annotation.StringRes
import android.support.v4.app.Fragment
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import cc.cloudist.acplibrary.ACProgressConstant
import cc.cloudist.acplibrary.ACProgressFlower


/**
 * Created by Heimdallr on 26.09.2017.
 */
abstract class BaseController : Fragment() {
    private var progressDialog: Dialog? = null

    fun showError(@StringRes id: Int) {
        if (id > 0) {
            Toast.makeText(activity, id, Toast.LENGTH_SHORT).show()
        }
    }

    fun showProgress(@StringRes id: Int) {
        hideProgress()
        if (id > 0) {
            progressDialog = ACProgressFlower.Builder(activity)
                    .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                    .themeColor(Color.WHITE)
                    .text(getString(id))
                    .fadeColor(Color.DKGRAY).build()
            progressDialog!!.setCancelable(false)
            progressDialog!!.show()
        }
    }

    fun showProgress() {
        hideProgress()
        progressDialog = ACProgressFlower.Builder(activity)
                .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                .themeColor(Color.WHITE)
                .fadeColor(Color.DKGRAY).build()
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    fun hideProgress() {
        if (progressDialog?.isShowing == true) {
            progressDialog?.dismiss()
        }
    }

    fun showKeyboard() {
        val activity = activity ?: return
        val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
    }

    fun hideKeyboard() {
        val activity = activity ?: return
        val view = activity.currentFocus
        if (view != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        }
    }

    fun getResolver(): ContentResolver = activity!!.contentResolver
}