package ru.fate.imagegrabber.android.common

import android.os.Bundle
import android.view.View

/**
 * Created by Heimdallr on 26.09.2017.
 */
abstract class BaseControllerWithPresenter<Presenter : BasePresenter<*, *>> : BaseController() {
    var presenter: Presenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onStart() {
        super.onStart()
        //  prepareUi()
        presenter?.onStart()
    }

    override fun onStop() {
        presenter?.onStop()
        hideProgress()
        super.onStop()
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        prepareUi()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString(BUNDLE_PRESENTER_CLASS_NAME, presenter?.javaClass?.name ?: "")
        outState.putString(BUNDLE_ROUTER_CLASS_NAME, presenter?.router?.javaClass?.name ?: "")

        presenter?.onSaveInstanceState(outState)
    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        if (savedInstanceState != null) {
            val presenterClassName = savedInstanceState.getString(BUNDLE_PRESENTER_CLASS_NAME, null)
            val routerClassName = savedInstanceState.getString(BUNDLE_ROUTER_CLASS_NAME, null)

            try {
                val clazzRouter = Class.forName(routerClassName)
                val ctor2 = clazzRouter.constructors[0]
                val routerObj = ctor2.newInstance(activity)


                val clazz = Class.forName(presenterClassName)
                val ctor = clazz.constructors[0]
                val `object` = ctor.newInstance(this, routerObj)
                presenter = `object` as Presenter
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            presenter?.onRestoreInstanceState(savedInstanceState)
        }
    }

    open fun prepareUi() = Unit

    companion object {
        private val BUNDLE_PRESENTER_CLASS_NAME = "BUNDLE_PRESENTER_CLASS_NAME"
        private val BUNDLE_ROUTER_CLASS_NAME = "BUNDLE_ROUTER_CLASS_NAME"
    }
}