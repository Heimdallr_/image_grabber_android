package ru.fate.imagegrabber.android.server.models

import com.google.gson.annotations.SerializedName
import ru.fate.imagegrabber.android.server.models.common.BaseEntityResp

/**
 * Created by Heimdallr on 24.07.2017.
 */
class CommentResp : BaseEntityResp() {
    @SerializedName("image_id") //id объекта которому принадлежит комментарий
    lateinit var mediaId: String

    @SerializedName("author_id")
    var authorId: Long = 0

    @SerializedName("author")
    var authorName: String? = null

    @SerializedName("comment")
    lateinit var text: String

    @SerializedName("datetime")
    var publicationDate: Long = 0
}