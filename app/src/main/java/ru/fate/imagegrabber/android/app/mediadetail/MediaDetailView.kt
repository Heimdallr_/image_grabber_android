package ru.fate.imagegrabber.android.app.mediadetail

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.layout_toolbar_with_title.view.*
import kotlinx.android.synthetic.main.view_media_detail.view.*
import ru.fate.imagegrabber.android.R
import ru.fate.imagegrabber.android.common.BaseControllerWithPresenter

class MediaDetailView : BaseControllerWithPresenter<MediaDetailPresenter>() {
    var mediaId: String = ""
    var isAlbum: Boolean = false

    companion object {
        fun newInstance(mediaId: String, isAlbum: Boolean): MediaDetailView {
            val args = Bundle()
            args.putString("mediaId", mediaId)
            args.putBoolean("isAlbum", isAlbum)

            val fragment = MediaDetailView()
            fragment.arguments = args
            return fragment
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true

        val bundle = arguments
        if (bundle != null) {
            mediaId = bundle.getString("mediaId")
            isAlbum = bundle.getBoolean("isAlbum")
        }
    }

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? =
            inflater?.inflate(R.layout.view_media_detail, container, false)

    //region Реализованные методы вида
    fun setMediaContentVisibility(visible: Boolean) = if (visible) {
        view?.mediaContent?.visibility = View.VISIBLE
    } else {
        view?.mediaContent?.visibility = View.GONE
    }

    fun setMediaInfo(title: String?, description: String?, viewsCount: Long, imageUrl: String) {
        view?.toolbarTitle?.text = title

        Glide.with(activity!!)
                .load(imageUrl)
                .into(view?.ivPhoto!!)

        view?.tvTitle?.text = title
        view?.tvDescription?.text = description
        view?.tvViewsCount?.text = viewsCount.toString()

        view?.llTitleContainer?.visibility = if (title.isNullOrBlank()) View.GONE else View.VISIBLE
        view?.llDescriptionContainer?.visibility = if (description.isNullOrBlank()) View.GONE else View.VISIBLE
        view?.llViewsCountContainer?.visibility = View.VISIBLE
    }

    fun setCommentsAdapter(adapter: RecyclerView.Adapter<*>) {
        view?.recyclerComments?.adapter = adapter
    }
    //endregion

    //region Приватные методы
    override fun prepareUi() {
        view?.toolbar?.setNavigationIcon(R.drawable.ic_back)
        view?.toolbar?.setNavigationOnClickListener { activity.onBackPressed() }

        view?.recyclerComments?.let {
            it.itemAnimator = null
            it.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)

            val dividerItemDecoration = DividerItemDecoration(it.context, LinearLayoutManager.VERTICAL)
            dividerItemDecoration.setDrawable(ContextCompat.getDrawable(activity!!, R.drawable.comments_divider))
            it.addItemDecoration(dividerItemDecoration)
        }
    }
    //endregion
}